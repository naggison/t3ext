<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Nagusch.' . $_EXTKEY,
	'Ewathreecols',
	array(
		'ThreeColRow' => 'list, show',
		'ThreeCol' => 'list, show',
		
	),
	// non-cacheable actions
	array(
		'ThreeColRow' => '',
		'ThreeCol' => '',
		
	)
);
