<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Ewathreecols',
	'espeythreecols'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'ThreeCols');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_treecolsewa_domain_model_threecolrow', 'EXT:treecolsewa/Resources/Private/Language/locallang_csh_tx_treecolsewa_domain_model_threecolrow.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_treecolsewa_domain_model_threecolrow');
$GLOBALS['TCA']['tx_treecolsewa_domain_model_threecolrow'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:treecolsewa/Resources/Private/Language/locallang_db.xlf:tx_treecolsewa_domain_model_threecolrow',
		'label' => 'threecolumn',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'threecolumn,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/ThreeColRow.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_treecolsewa_domain_model_threecolrow.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_treecolsewa_domain_model_threecol', 'EXT:treecolsewa/Resources/Private/Language/locallang_csh_tx_treecolsewa_domain_model_threecol.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_treecolsewa_domain_model_threecol');
$GLOBALS['TCA']['tx_treecolsewa_domain_model_threecol'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:treecolsewa/Resources/Private/Language/locallang_db.xlf:tx_treecolsewa_domain_model_threecol',
		'label' => 'headline',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'headline,bodytext,image,imagesubline,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/ThreeCol.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_treecolsewa_domain_model_threecol.gif'
	),
);
